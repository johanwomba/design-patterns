package com.FactoryPattern;

public class CarFactory {
	
	public static Car buildCar(CarType model) {
		Car car = null;
		
		switch (model) {
			
		case COUPE:
			car = new CoupeCar();
			break;
		case SEDAN:
			car = new SedanCar();
			break;
		case SPORTS:
			car = new SportsCar();
			break;
		case LUXURY:
			car = new LuxuryCar();
			break;
		default:
			break;
		}
		return car;
	}

}
