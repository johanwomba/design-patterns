package com.FactoryPattern;

public class LuxuryCar extends Car {
	
	public LuxuryCar() {
		super(CarType.LUXURY);
		construct();
	}
	
	protected void construct() {
		System.out.println("Building an EPIC ride!");
	}

}
