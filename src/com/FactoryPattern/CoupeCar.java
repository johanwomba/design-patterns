package com.FactoryPattern;

public class CoupeCar extends Car {

	public CoupeCar() {
		super(CarType.COUPE);
		construct();
	}
	
	protected void construct() {
		System.out.println("Building a little Coupe.");
	}
}
