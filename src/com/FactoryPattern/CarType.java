package com.FactoryPattern;

public enum CarType {
	
	COUPE,
	SEDAN,
	SPORTS,
	LUXURY
}