package com.FactoryPattern;

public class Main {

	public static void main(String[] args) {
		System.out.println(CarFactory.buildCar(CarType.COUPE));
		System.out.println(CarFactory.buildCar(CarType.LUXURY));
		System.out.println(CarFactory.buildCar(CarType.SPORTS));
		System.out.println(CarFactory.buildCar(CarType.SEDAN));
	}

}
