package com.FactoryPattern;

public class SportsCar extends Car {
	
	public SportsCar () {
		super(CarType.SPORTS);
		construct();
	}

	protected void construct() {
		System.out.println("Putting together a Sports Car.");
	}
}