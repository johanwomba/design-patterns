package com.FactoryPattern;

public class SedanCar extends Car {

	public SedanCar() {
		super(CarType.SEDAN);
		construct();
	}
	
	protected void construct() {
		System.out.println("Building a sedan.");
	}
}
