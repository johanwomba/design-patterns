package com.CompositionPattern;

public class Main {

	public static void main(String[] args) {
		
		Person person = new Person();
		
		System.out.println("I am " + person.getJob().getRole());
		System.out.println("I make about " + person.getJob().getSalary() + " $/h.");

	}

}
