package com.CompositionPattern;

public class Person {
	
	private Job job;
	
	public Person() {
		this.job = new Job();
		job.setSalary(122.154);
		job.setRole("Commander-in-Chief");
		job.setId(666);
	}
	
	public double getSalary() {
        return job.getSalary();
    }

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}
	
}
