package com.CollectionPattern;

import java.util.Iterator;

public class PlayChannelIterator implements Iterator<String> {
	
	String[] channelList;
	int position = 0;
 
	public PlayChannelIterator(String[] list) {
		this.channelList = list;
	}
 
	public String next() {
		String channel = channelList[position];
		position += 1;
		return channel;
	}
 
	public boolean hasNext() {
		if (position >= channelList.length || channelList[position] == null) {
			return false;
		} else {
			return true;
		}
	}
  
	public void remove() {

		if (channelList[position-1] != null) {
			for (int i = position-1; i < (channelList.length-1); i++) {
				channelList[i] = channelList[i+1];
			}
			channelList[channelList.length-1] = null;
		}
	}
}