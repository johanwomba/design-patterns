package com.CollectionPattern;

import java.util.Iterator;

public class Main {

	public static void main(String args[]) {
		PlayChannel playChannel = new PlayChannel();
		TVChannel tvChannel = new TVChannel();
		
		// with iterators
		Iterator<String> playChannelIterator = playChannel.createIterator();
		Iterator<String>  tvChannelIterator = tvChannel.createIterator();

		System.out.println("PLAY CHANNELS");
		printMenu(playChannelIterator);
		System.out.println("\nTV CHANNELS");
		printMenu(tvChannelIterator); 
	}
 
	private static void printMenu(Iterator<String> iterator) {
		while (iterator.hasNext()) {
			String menuItem = (String)iterator.next();
			System.out.println(menuItem);

		}
	}
}
