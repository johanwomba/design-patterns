package com.CollectionPattern;

import java.util.Iterator;

public class PlayChannel implements Guide {

	static final int MAX_CHANNELS = 5;
	int itemCounter = 0;
	String [] playChannels;
	
	public PlayChannel () {
		playChannels = new String[MAX_CHANNELS];
		
		addChannel("Cmore Action");
		addChannel("Discovery");
		addChannel("TV3 Play");
		addChannel("TV4 Play");
		addChannel("TV8 Play");
	}
	
	public void addChannel(String name) {
		
		if (itemCounter >= MAX_CHANNELS) {
			System.out.println("The Play Channel list is full.");
		}
		else {
			playChannels[itemCounter] = name;
			itemCounter += 1;
		}
	}

	public String[] getPlayChannels() {
		return playChannels;
	}
	
	public Iterator<String> createIterator() {
		return new PlayChannelIterator(playChannels);
	}
	public String toString() {
		return "Play Channels";
	}
}