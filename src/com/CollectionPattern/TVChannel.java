package com.CollectionPattern;

import java.util.ArrayList;
import java.util.Iterator;

public class TVChannel implements Guide {

	ArrayList<String> TVChannels;
	
	public TVChannel() {
		TVChannels = new ArrayList<String>();
		
		TVChannels.add("SVT1");
		TVChannels.add("SVT2");
		TVChannels.add("Discovery");
		TVChannels.add("TV3");
		TVChannels.add("TV10");
	}

	public ArrayList<String> getTVChannels() {
		return TVChannels;
	}
	
	public Iterator<String> createIterator() {
		return TVChannels.iterator();
	}
	
	public String toString() {
		return "TV Channels";
	}
	
}