package com.CollectionPattern;

import java.util.Iterator;

public interface Guide {

	public Iterator<String> createIterator();
}