package com.SingletonPattern.threadSafe;

public class Singleton {

	private static Singleton oneOfaKind;
	
	private Singleton() { }
	
	public static synchronized Singleton getInstance() { //can be expensive depending on the application.
		
		if (oneOfaKind == null) {
			oneOfaKind = new Singleton();
		}
		return oneOfaKind;
	}
	
	public void sayHello() {
		
		System.out.println("Hey! I'm lonely.");
	}
}