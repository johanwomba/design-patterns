package com.SingletonPattern;

public class Singleton {

	private static Singleton oneOfaKind;
	
	private Singleton() { }
	
	public static Singleton getInstance() {
		
		if (oneOfaKind == null) {
			oneOfaKind = new Singleton();
		}
		return oneOfaKind;
	}
	
	public void sayHello() {
		
		System.out.println("Hey! I'm lonely.");
	}
}