package com.SingletonPattern;

public class Main {
	
	public static void main (String[]args) {
	
		Singleton soRonery = Singleton.getInstance();
		soRonery.sayHello();
		
	}
}