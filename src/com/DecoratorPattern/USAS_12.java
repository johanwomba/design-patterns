package com.DecoratorPattern;

public class USAS_12 extends Weapon {
	
	public USAS_12() {
		name = "USAS-12";
		description = "Automatic shotgun";
	}

	public double cost() {
		return 599;
	}

}