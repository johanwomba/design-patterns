package com.DecoratorPattern;

public class Lasersight extends WeaponDecorator {

	Weapon weapon;
	
	public Lasersight(Weapon weapon) {
		this.weapon = weapon;
	}
	
	public String getDescription() {
		return weapon.getDescription() + ", Lasersight";
	}

	public double cost() {
		return weapon.cost() + 29.99;
	}	
}