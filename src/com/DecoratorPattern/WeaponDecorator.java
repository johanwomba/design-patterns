package com.DecoratorPattern;

public abstract class WeaponDecorator extends Weapon {

	public abstract String getDescription();
}
