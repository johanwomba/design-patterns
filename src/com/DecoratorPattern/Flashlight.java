package com.DecoratorPattern;

public class Flashlight extends WeaponDecorator {

	Weapon weapon;
	
	public Flashlight(Weapon weapon) {
		this.weapon = weapon;
	}
	
	public String getDescription() {
		return weapon.getDescription() + ", Flashlight";
	}

	public double cost() {
		return weapon.cost() + 49.99;
	}	
}