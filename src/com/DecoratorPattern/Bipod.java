package com.DecoratorPattern;

public class Bipod extends WeaponDecorator {

	Weapon weapon;
	
	public Bipod (Weapon weapon) {
		this.weapon = weapon;
	}
	
	public String getDescription() {
		return weapon.getDescription() + ", Bipod";
	}

	public double cost() {
		return weapon.cost() + 99.99;
	}	
}
