package com.DecoratorPattern;

public class ACE_52_CQB extends Weapon {

	public ACE_52_CQB() {
		name = "ACE 52 CQB";
		description = "Automatic/single shot carbine";
	}
	public double cost() {
		return 350;
	}

}