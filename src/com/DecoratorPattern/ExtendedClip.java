package com.DecoratorPattern;

public class ExtendedClip extends WeaponDecorator {

	Weapon weapon;
	
	public ExtendedClip(Weapon weapon) {
		this.weapon = weapon;
	}
	
	public String getDescription() {
		return weapon.getDescription() + ", Extended Clip";
	}

	public double cost() {
		return weapon.cost() + 79.99;
	}	
}