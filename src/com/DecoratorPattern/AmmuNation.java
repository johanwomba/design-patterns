package com.DecoratorPattern;

public class AmmuNation {

	public static void main(String[] args) {
	
		/*Accessories;
		*Bipod
		*Extended Clip
		*Lasersight
		*/
		Weapon weapon = new F2000();
		System.out.println(weapon.getDescription() + " $" + weapon.cost());
		
		Weapon weapon2 = new ACE_52_CQB();
		weapon2 = new Flashlight(weapon2);
		System.out.println(weapon2.getDescription() + " $" + weapon2.cost());
		
		Weapon weapon3 = new USAS_12();
		weapon3 = new Lasersight(weapon3);
		weapon3 = new Bipod(weapon3);
		weapon3 = new ExtendedClip(weapon3);
		System.out.println(weapon3.getDescription() + " $" + weapon3.cost());

	}
}