package com.DecoratorPattern;

public abstract class Weapon {
	
	String name = "Unknown name";
	String description = "Unknown description";
	
	public String getDescription() {
		return name + " [" + description + "]";
	}
	public abstract double cost();

}