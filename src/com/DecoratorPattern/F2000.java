package com.DecoratorPattern;

public class F2000 extends Weapon {

	public F2000() {
		name = "F2000";
		description = "Automatic rifle";
	}
	
	public double cost() {
		return 399.99;
	}
	
}