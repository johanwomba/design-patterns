package com.StrategyPattern;

public class FreshWater implements WaterPreference {

	public void swim() {

		System.out.println("I'm swimming in a lake!");
	}
}