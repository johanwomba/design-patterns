package com.StrategyPattern;

public class LoneWolf implements SchoolBehavior {

	public void hunt() {
		
		System.out.println("I like to hunt alone.");
	}
}