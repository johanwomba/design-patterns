package com.StrategyPattern;

public class Pike extends Fish {


	public Pike() {
		
		waterPreference = new FreshWater();
		schoolBehavior = new LoneWolf();
		
	}
	public void display() {
		
		System.out.println("I'm a pike! I can grow to be over 30kg!");
	}	
}