package com.StrategyPattern;

public interface WaterPreference {
	
	public void swim();
}