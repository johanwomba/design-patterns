package com.StrategyPattern;

public class Together implements SchoolBehavior {

	public void hunt() {
		
		System.out.println("I often hunt with my buddies.");
	}
}