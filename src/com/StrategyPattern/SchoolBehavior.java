package com.StrategyPattern;

public interface SchoolBehavior {

	public void hunt();
}
