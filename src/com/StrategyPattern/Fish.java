package com.StrategyPattern;

public abstract class Fish {
	
	SchoolBehavior schoolBehavior;
	WaterPreference waterPreference;
	
	abstract void display();
	
	public void letsHunt() {
		schoolBehavior.hunt();
	}
	
	public void whereAmI() {
		waterPreference.swim();
	}
	
	public void eat() {
		System.out.println("Nom nom nom. I'm a fish, I eat everything.");
	}
	
	public SchoolBehavior getSchoolBehavior() {
		return schoolBehavior;
	}
	public void setSchoolBehavior(SchoolBehavior schoolBehavior) {
		this.schoolBehavior = schoolBehavior;
	}
	public WaterPreference getWaterPreference() {
		return waterPreference;
	}
	public void setWaterPreference(WaterPreference waterPreference) {
		this.waterPreference = waterPreference;
	}
}