package com.StrategyPattern;

public class Perch extends Fish {
	
	public Perch() {
		waterPreference = new FreshWater();
		schoolBehavior = new Together();
	}

	void display() {
		
		System.out.println("I'm a perch! Touch me and you'll get stabbed, cabron.");
	}
}