package com.StrategyPattern;

public class Salmon extends Fish {

	public Salmon() {
		
		waterPreference = new SaltWater();
		schoolBehavior = new Together();
	}

	void display() {
		
		System.out.println("I'm a salmon! My favorite song is Jump Around.");
	}
}