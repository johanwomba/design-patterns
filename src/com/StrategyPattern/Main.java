package com.StrategyPattern;

public class Main {

	public static void main (String [] args) {

		Pike gadda = new Pike();
		Perch abborre = new Perch();
		Salmon lax = new Salmon();
		
		gadda.display();
		gadda.eat();
		gadda.letsHunt();
		gadda.whereAmI();
		
		System.out.println("");
		lax.display();
		lax.eat();
		lax.letsHunt();
		lax.whereAmI();
		
		System.out.println("");
		abborre.display();
		abborre.eat();
		abborre.letsHunt();
		abborre.whereAmI();
		
		abborre.setSchoolBehavior(new LoneWolf());
		System.out.println("Changed my mind!");
		abborre.letsHunt();

	}
}