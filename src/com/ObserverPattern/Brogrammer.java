package com.ObserverPattern;

import java.util.Observable;
import java.util.Observer;

public class Brogrammer implements Observer, DisplayElement {

	private String subject;
	private String message;

	public Brogrammer(Observable observable) {
		observable.addObserver(this);
	}

	public void update(Observable obs, Object arg) {
		if (obs instanceof NewsletterSender) {
			NewsletterSender newsletter = (NewsletterSender)obs;
			this.subject = newsletter.getSubject().toLowerCase();
			this.message = newsletter.getMessage().toLowerCase();
			display();
		}
	}

	public void display() {
		if (this.subject.contains("programming")) {
			System.out.printf("Brogrammer says: I dont even care what the message is, I'm gonna read it anyways!\n");
		}
		else if (this.message.contains("xxx")) {
			System.out.printf("Brogrammer says: Friggin' spam.\n");
		}
		else {
			System.out.println("Brogrammer says: Booooring.");
		}
	}
}