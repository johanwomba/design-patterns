package com.ObserverPattern;

public interface DisplayElement {
	public void display();
}
