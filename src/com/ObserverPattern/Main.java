package com.ObserverPattern;


public class Main {

	public static void main(String[] args) {
		
		//Subject
		NewsletterSender newsLetter = new NewsletterSender();
		
		//Observers
		Perv perv = new Perv(newsLetter);
		Brogrammer brogrammer = new Brogrammer(newsLetter);
		GourmetChef chef = new GourmetChef(newsLetter);
		newsLetter.setMessage("Programming", "blablabla");
		newsLetter.setMessage("Cooking", "blablabla");
		newsLetter.setMessage("Adult Stuff", "blablaxxxbla");
	}
}