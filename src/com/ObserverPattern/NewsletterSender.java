package com.ObserverPattern;

import java.util.Observable;
	
public class NewsletterSender extends Observable {
	
	private String subject;
	private String message;
	
	public void newNewsletter() {
		setChanged();
		notifyObservers();
	}
	
	public void setMessage(String subject, String message) {
		this.subject = subject;
		this.message = message;
		newNewsletter();
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}