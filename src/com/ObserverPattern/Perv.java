package com.ObserverPattern;

import java.util.Observable;
import java.util.Observer;
	
public class Perv implements Observer, DisplayElement {
	
	private String subject;
	private String message;
	
	public Perv(Observable observable) {
		observable.addObserver(this);
	}
	
	public void update(Observable obs, Object arg) {
		if (obs instanceof NewsletterSender) {
			NewsletterSender newsletter = (NewsletterSender)obs;
			this.subject = newsletter.getSubject().toLowerCase();
			this.message = newsletter.getMessage().toLowerCase();
			display();
		}
	}
	
	public void display() {
		System.out.printf("Perv says: ");
		if (this.message.contains("xxx") && this.subject.contains("adult")) {
			System.out.printf("Sweeeet\n");
		}
		else {
			System.out.printf("...Boooring.\n");
		}
	}
}
