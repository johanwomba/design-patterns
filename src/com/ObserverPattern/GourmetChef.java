package com.ObserverPattern;

import java.util.Observable;
import java.util.Observer;

public class GourmetChef implements Observer, DisplayElement {
	
	private String subject;

	public GourmetChef(Observable observable) {
		observable.addObserver(this);
	}

	public void update(Observable obs, Object arg) {
		
		if (obs instanceof NewsletterSender) {
			NewsletterSender newsletter = (NewsletterSender)obs;
			this.subject = newsletter.getSubject().toLowerCase();
			display();
		}
	}

	public void display() {
		if (subject.contains("cooking")) {
			System.out.println("Gourmet Chef says: Yum!\n");
		}
	}
}
