package com.StatePattern;

public class SoldOutState implements State {

	CokeMachine cokeMachine;
	
	public SoldOutState (CokeMachine cokeMachine) {
		this.cokeMachine = cokeMachine;
	}

	public void insertCoin() {
		System.out.println("We're unfortunately sold out :c");
		
	}

	public void ejectCoin() {
		System.out.println("You can't eject your coin, since you haven't inserted one yet.");
		
	}

	public void orderUp() {
		System.out.println("You ordered, but there are no cokes.");
		
	}
	
	public void dispense() {
		System.out.println("No coke dispensed...");
		
	}
	
	public String toString() {
		return "sold out";
	}
}