package com.StatePattern;

public class HasCoinState implements State {

	CokeMachine cokeMachine;
	
	public HasCoinState(CokeMachine cokeMachine) {
		this.cokeMachine = cokeMachine;
	}

	public void insertCoin() {
		System.out.println("You cant insert another coin.");
		
	}

	public void ejectCoin() {
		System.out.println("Coin returned.");
		cokeMachine.setState(cokeMachine.getNoCoinState());
	}

	public void orderUp() {
		System.out.println("You ordered.");
		cokeMachine.setState(cokeMachine.getSoldState());
	}

	public void dispense() {
		System.out.println("No coke dispensed.");
	}
	
	public String toString() {
		return "waiting for order.";
	}
	
}