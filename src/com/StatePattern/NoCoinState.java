package com.StatePattern;

public class NoCoinState implements State {

	CokeMachine cokeMachine;
	
	public NoCoinState (CokeMachine cokeMachine) {
		this.cokeMachine = cokeMachine;
	}
	
	public void insertCoin() {
		System.out.println("You inserted a coin.");
		cokeMachine.setState(cokeMachine.getHasCoinState());
		
	}

	public void ejectCoin() {
		System.out.println("You haven't inserted a coin.");
		
	}

	public void orderUp() {
		System.out.println("You tried to order without paying. No can do!");
		
	}
	
	public void dispense() {
		System.out.println("You need to pay first..");
		
	}

	public String toString() {
		return "waiting for coin.";
	}
}
