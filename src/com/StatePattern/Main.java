package com.StatePattern;

public class Main {

	public static void main (String [] args) {
		
		CokeMachine cokeMachine = new CokeMachine(2); //starts with n Coke cans.
		
		System.out.println(cokeMachine);
		
		cokeMachine.insertCoin();
		cokeMachine.orderUp();
		
		System.out.println(cokeMachine);
		
		cokeMachine.insertCoin();
		cokeMachine.orderUp();
		
		//Coke machine is sold out, but try to buy one anyway.
		System.out.println(cokeMachine);
		
		cokeMachine.insertCoin();
		cokeMachine.orderUp();
		
	}
}