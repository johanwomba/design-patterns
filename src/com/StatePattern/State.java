package com.StatePattern;

public interface State {

	public void insertCoin();
	public void ejectCoin();
	public void orderUp();
	public void dispense();
}
