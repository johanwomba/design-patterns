package com.StatePattern;

public class SoldState implements State {

	CokeMachine cokeMachine;
	
	public SoldState (CokeMachine cokeMachine) {
		this.cokeMachine = cokeMachine;
	}

	public void insertCoin() {
		System.out.println("Please wait, you're already getting your coke!");
		
	}

	public void ejectCoin() {
		System.out.println("Didn't you already order?");
		
	}

	public void orderUp() {
		System.out.println("Ordering twice wont get you anywhere..");
		
	}
	
	public void dispense() {
		cokeMachine.releaseCoke();
		
		if (cokeMachine.getCount() > 0) {
			cokeMachine.setState(cokeMachine.getNoCoinState());
		}
		else {
			System.out.println("We're all out!");
			cokeMachine.setState(cokeMachine.getSoldOutState());
		}
	}
	public String toString() {
		return "rolling out a coke.";
	}
	
}