package com.StatePattern;

public class CokeMachine {

	State soldOutState;
	State noCoinState;
	State hasCoinState;
	State soldState;
	
	State state = soldOutState;
	int count = 0;
	
	public CokeMachine(int numberCokes) {
		soldOutState = new SoldOutState(this);
		noCoinState = new NoCoinState(this);
		hasCoinState = new HasCoinState(this);
		soldState = new SoldState(this);
		
		this.count = numberCokes;
		
		if (numberCokes > 0) {
			state = noCoinState;
		}
	}
	
	public void insertCoin() {
		state.insertCoin();
	}
	
	public void ejectCoin() {
		state.ejectCoin();
	}
	
	public void orderUp() {
		state.orderUp();
		state.dispense();
	}
	
	void releaseCoke() {
		System.out.println("A Coke is rolling out..");
		if (count != 0) {
			count -= 1;
		}
	}
	
	void refill (int count) {
		this.count = count;
		state = noCoinState;
	}

	public State getSoldOutState() {
		return soldOutState;
	}

	public void setSoldOutState(State soldOutState) {
		this.soldOutState = soldOutState;
	}

	public State getNoCoinState() {
		return noCoinState;
	}

	public void setNoCoinState(State noCoinState) {
		this.noCoinState = noCoinState;
	}

	public State getHasCoinState() {
		return hasCoinState;
	}

	public void setHasCoinState(State hasCoinState) {
		this.hasCoinState = hasCoinState;
	}

	public State getSoldState() {
		return soldState;
	}

	public void setSoldState(State soldState) {
		this.soldState = soldState;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append("\nCoke Machine");
		result.append("\nModel #2090.4.1s");
		result.append("\nInventory: " + count + " Coke");
		if (count != 1) {
			result.append("s");
		}
		result.append("\n");
		result.append("Machine is " + state + "\n");
		return result.toString();
	}
}